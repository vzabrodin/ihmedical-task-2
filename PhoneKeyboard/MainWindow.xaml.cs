﻿using System;
using System.Windows;
using System.Windows.Threading;
using PhoneKeyboard.UserControls;

namespace PhoneKeyboard
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private DispatcherTimer timer;
		private KeyboardController kctrl;
		private KeyboardButton btnPrevioslyPressed = null;

		public MainWindow()
		{
			timer = new DispatcherTimer();
			timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
			timer.Tick += timer_Tick;

			kctrl = new KeyboardController();

			InitializeComponent();

			lblCaps.Text = kctrl.IsCaps ? "&#11014;" : "";
			lblLanguage.Text = kctrl.Language.ToString();
			tbMain.Focus();
		}

		private void ClearSelection()
		{
			tbMain.SelectionStart += tbMain.SelectedText.Length;
			tbMain.SelectionLength = 0;
		}

		#region Event handlers

		private void timer_Tick(object sender, EventArgs e)
		{
			tbMain.SelectionStart += tbMain.SelectedText.Length;
			tbMain.SelectionLength = 0;
			timer.Stop();
		}

		private void btnDigit_Click(object sender, EventArgs e)
		{
			try
			{
				KeyboardButton btn = sender as KeyboardButton;

				if (btn != btnPrevioslyPressed)
				{
					ClearSelection();
				}

				tbMain.SelectedText = kctrl.IsCaps
					? char.ToUpperInvariant(kctrl[int.Parse(btn.Digit), btn.ClickCount - 1]).ToString()
					: kctrl[int.Parse(btn.Digit), btn.ClickCount - 1].ToString();
				timer.Stop();
				timer.Start();
				btnPrevioslyPressed = btn;
			}
			catch
			{
				// ignored
			}
		}

		private void btnDigit_LongClick(object sender, EventArgs e)
		{
			string text = (sender as KeyboardButton).Digit;
			tbMain.SelectedText = text;
			tbMain.SelectionStart += tbMain.SelectedText.Length;
			tbMain.SelectionLength = 0;
		}

		private void btnChangeLang_Click(object sender, EventArgs e)
		{
			kctrl.Language = (KeyboardController.KeyboardLanguage)(((int)kctrl.Language + 1) % kctrl.LanguageCount);
			lblLanguage.Text = kctrl.Language.ToString();
		}

		private void btnSetCaps_Click(object sender, EventArgs e)
		{
			kctrl.IsCaps = !kctrl.IsCaps;
			lblCaps.Text = kctrl.IsCaps ? ((char)11014).ToString() : "";
		}

		private void btnCursorLeft_Click(object sender, RoutedEventArgs e)
		{
			if (tbMain.SelectionStart > 0)
			{
				tbMain.SelectionStart--;
			}
			else
			{
				tbMain.SelectionStart = tbMain.Text.Length;
			}
			tbMain.SelectionLength = 0;
		}

		private void btnCursorRight_Click(object sender, RoutedEventArgs e)
		{
			tbMain.SelectionStart = (tbMain.SelectionStart + 1) % (tbMain.Text.Length + 1);
			tbMain.SelectionLength = 0;
		}

		#endregion

		private void btnBackspace_Click(object sender, RoutedEventArgs e)
		{
			if (tbMain.SelectionStart > 0)
			{
				ClearSelection();
				tbMain.SelectionStart--;
				tbMain.SelectionLength = 1;
				tbMain.SelectedText = "";
			}
		}
	}
}