﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace PhoneKeyboard.UserControls
{
	/// <summary>
	/// Логика взаимодействия для KeyboardButton.xaml
	/// </summary>
	public partial class KeyboardButton : UserControl
	{
		public DispatcherTimer timerLongClick;
		public DispatcherTimer timerClick;

		public event EventHandler Click;
		public event EventHandler LongClick;

		public KeyboardButton()
		{
			timerClick = new DispatcherTimer();
			timerLongClick = new DispatcherTimer();
			timerClick.Interval = new TimeSpan(0, 0, 0, 0, ClickDelay);
			timerLongClick.Interval = new TimeSpan(0, 0, 0, 0, LongClickDelay);
			timerClick.Tick += timerClick_Tick;
			timerLongClick.Tick += timerLongClick_Tick;

			ClickCount = 0;

			InitializeComponent();
		}

		#region Event handlers

		private void timerClick_Tick(object sender, EventArgs e)
		{
			ClickCount = 0;
			timerClick.Stop();
		}

		private void timerLongClick_Tick(object sender, EventArgs e)
		{
			timerLongClick.Stop();
			if (LongClick != null)
			{
				LongClick(this, EventArgs.Empty);
			}
		}

		private void button_MouseDown(object sender, MouseButtonEventArgs e)
		{
			timerClick.Stop();
			timerClick.Start();

			timerLongClick.Start();
			e.Handled = true;
		}

		private void button_MouseUp(object sender, MouseButtonEventArgs e)
		{
			if (timerClick.IsEnabled)
			{
				ClickCount++;
				if (Click != null)
				{
					Click(this, EventArgs.Empty);
				}
			}
			timerLongClick.Stop();
			e.Handled = true;
		}

		#endregion

		#region Properties

		public int ClickCount { get; private set; }

		public int ClickDelay
		{
			get
			{
				return (int)GetValue(ClickDelayProperty);
			}
			set
			{
				timerLongClick.Interval = new TimeSpan(0, 0, 0, 0, value);
				SetValue(ClickDelayProperty, value);
			}
		}

		public static readonly DependencyProperty ClickDelayProperty =
			DependencyProperty.Register("ClickDelay", typeof(int), typeof(KeyboardButton), new UIPropertyMetadata(500));

		public int LongClickDelay
		{
			get
			{
				return (int)GetValue(LongClickDelayProperty);
			}
			set
			{
				timerLongClick.Interval = new TimeSpan(0, 0, 0, 0, value);
				SetValue(LongClickDelayProperty, value);
			}
		}

		public static readonly DependencyProperty LongClickDelayProperty =
			DependencyProperty.Register("LongClickDelay", typeof(int), typeof(KeyboardButton), new UIPropertyMetadata(500));

		public string Chars
		{
			get
			{
				return (string)GetValue(CharsProperty);
			}
			set
			{
				SetValue(CharsProperty, value);
			}
		}

		public static readonly DependencyProperty CharsProperty =
			DependencyProperty.Register("Chars", typeof(string), typeof(KeyboardButton), new UIPropertyMetadata(""));

		public string CharsPrimary
		{
			get
			{
				return (string)GetValue(CharsPrimaryProperty);
			}
			set
			{
				SetValue(CharsPrimaryProperty, value);
			}
		}

		public static readonly DependencyProperty CharsPrimaryProperty =
			DependencyProperty.Register("CharsPrimary", typeof(string), typeof(KeyboardButton), new UIPropertyMetadata(""));

		public string CharsSecondary
		{
			get
			{
				return (string)GetValue(CharsSecondaryProperty);
			}
			set
			{
				SetValue(CharsSecondaryProperty, value);
			}
		}

		public static readonly DependencyProperty CharsSecondaryProperty =
			DependencyProperty.Register("CharsSecondary", typeof(string), typeof(KeyboardButton), new UIPropertyMetadata(""));

		public string Digit
		{
			get
			{
				return (string)GetValue(DigitProperty);
			}
			set
			{
				SetValue(DigitProperty, value);
			}
		}

		public static readonly DependencyProperty DigitProperty =
			DependencyProperty.Register("Digit", typeof(string), typeof(KeyboardButton), new UIPropertyMetadata(""));

		#endregion
	}
}