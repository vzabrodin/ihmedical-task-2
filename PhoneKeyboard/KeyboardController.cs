﻿using System;
using System.Collections.Generic;

namespace PhoneKeyboard
{
	class KeyboardController
	{
		public enum KeyboardLanguage { English, Russian }

		private Dictionary<KeyboardLanguage, char[][]> _languages;

		#region Properties

		public KeyboardLanguage Language { get; set; }

		public bool IsCaps { get; set; }

		public int LanguageCount
		{
			get
			{
				return _languages.Count;
			}
		}

		public char this[int digit, int count]
		{
			get
			{
				return _languages[Language][digit][count % _languages[Language][digit].Length];
			}
		}

		#endregion

		public KeyboardController()
		{
			Language = KeyboardLanguage.English;
			_languages = new Dictionary<KeyboardLanguage, char[][]>()
			{
				{
					KeyboardLanguage.English,
					new char[][] {
						new char[] { ' ', '0', '\n' },
						new char[] { '.', ',', '?', '!', '1', '@', '\'', '-', '_', '(', ')', ':', ';', '&', '/', '%', '*', '#', '+', '<', '=', '>', '\"' },
						new char[] { 'a', 'b', 'c', '2' },
						new char[] { 'd', 'e', 'f', '3' },
						new char[] { 'g', 'h', 'i', '4' },
						new char[] { 'j', 'k', 'l', '5' },
						new char[] { 'm', 'n', 'o', '6' },
						new char[] { 'p', 'q', 'r', 's', '7' },
						new char[] { 't', 'u', 'v', '8' },
						new char[] { 'w', 'x', 'y', 'z', '9' }
					}
				},
				{
					KeyboardLanguage.Russian,
					new char[][] {
						new char[] { ' ', '0', '\n' },
						new char[] { '.', ',', '?', '!', '1', '@', '\'', '-', '_', '(', ')', ':', ';', '&', '/', '%', '*', '#', '+', '<', '=', '>', '\"' },
						new char[] { 'а', 'б', 'в', 'г', '2' },
						new char[] { 'д', 'е', 'ё', 'ж', 'з', '3' },
						new char[] { 'и', 'й', 'к', 'л', '4' },
						new char[] { 'м', 'н', 'о', 'п', '5' },
						new char[] { 'р', 'с', 'т', 'у', '6' },
						new char[] { 'ф', 'х', 'ц', 'ч', '7' },
						new char[] { 'ш', 'щ', 'ъ', 'ы', '8' },
						new char[] { 'ь', 'э', 'ю', 'я', '9' }
					}
				}
			};
		}
	}
}